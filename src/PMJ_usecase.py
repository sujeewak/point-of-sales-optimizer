'''
    File name: PMJ_usecase.py
    Author: Sujeewa Kumaratunga
    Date created: 8/10/2020
    Date last modified: 8/10/2020
    Python Version: 3.6

    Pre-requisites:
        python 3.6
        directory structure:
        PMJ
        ├── data
        │   ├── Surroundings.json
        │   ├── sales_granular.csv
        └── src
           ├── PMJ_usecase.py

    Output:
       data/final_df_for_modeling.csv
       RMSE score from RidgeCV
       scatterplot of predicted vs true scores

'''


from pathlib import Path

import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MaxAbsScaler
from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import Ridge, RidgeCV


proj_path = Path(str(Path('__file__').absolute()).split('PMJ')[0]+'PMJ')

class PMJUseCase():

    def __init__(self,
                 sales_fpath, surrounding_fpath,
                 sparse_stores_thresh=0):

        #some paths
        self.plotpath = proj_path / 'figs'

        #dfs
        print('getting csv')
        self.df_sales = self.sales_df_getter(sales_fpath, sparse_stores_thresh)
        print('getting surr')
        self.df_surr = self.surroundings_df_getter(surrounding_fpath)
        self.shop_scores_final = pd.DataFrame() #set this in scroing function
        self.df_final_modeling = pd.DataFrame()
        self.traindf = pd.DataFrame()
        self.testdf = pd.DataFrame()
        self.train_n = pd.DataFrame() #normalized train df
        self.test_n = pd.DataFrame() #normalized test df
        self.feat_cols = []




    def sales_df_getter(self, sales_fpath, sparse_stores_thresh=0):
        df_sales = pd.read_csv(sales_fpath)
        df_sales.drop_duplicates(subset="store_code",
                             keep=False, inplace=True)

        if sparse_stores_thresh>0:
            ts_per_store = df_sales.set_index('store_code').count(axis=1)
            min_ts = list(ts_per_store[(ts_per_store >= sparse_stores_thresh)].keys())
            df_sales = df_sales[df_sales['store_code'].isin(min_ts)]

        return df_sales

    def surroundings_df_getter(self, surr_fpath):
        df = pd.read_json(surr_fpath, orient='records')
        df.drop_duplicates(subset="store_code",
                                        keep=False, inplace=True)

        dfnew = df['surroundings'].apply(pd.Series)
        for cc in dfnew.columns:
            dfnew[cc] = dfnew[cc].apply(lambda x: len(x))
        dfnew['store_code'] = df['store_code']

        return dfnew



    def score_stores(self):
        df_sales_t = self.df_sales.set_index('store_code').transpose()
        df_sales_t.index = pd.to_datetime(df_sales_t.index)

        df_sales_h = df_sales_t.copy()
        df_sales_h['hour'] = df_sales_h.index.hour
        dg_h = df_sales_h.groupby('hour').mean()

        time_brackets = [0, 8, 16, 24]
        ll_maxh = []
        for ii in range(1, len(time_brackets) - 1):
            print(time_brackets[ii], time_brackets[ii - 1])
            ll_maxh.append([])
        ll_maxh.append([])
        for cc in dg_h.columns:
            maxhr = dg_h[cc].idxmax()
            for ii in range(1, len(time_brackets)):
                if ((maxhr <= time_brackets[ii]) & (maxhr > time_brackets[ii - 1])):
                    ll_maxh[ii - 1].append(cc)

        # get averages of above dg_h[ll] groups
        shop_scores_g = []
        for ii, ll in enumerate(ll_maxh):
            dgtmp = dg_h[ll]
            if ii < len(time_brackets) - 1:
                dgtmp = dgtmp[time_brackets[ii] + 1:time_brackets[ii + 1] + 1]

            means_g = dgtmp.mean()
            shop_scores_g.append((means_g / means_g.mean()))
        means_o = dg_h.mean()
        shop_scores_o = means_o/means_o.mean()

        shop_scores_o = shop_scores_o.to_frame()
        shop_scores_o.head()
        shop_scores_o.columns = ['score_overall']

        shop_scores_finaldf = pd.DataFrame()
        for ii, ss in enumerate(shop_scores_g):
            dftmp = ss.to_frame()

            if ii == 0:
                shop_scores_finaldf = dftmp.copy()
            else:
                shop_scores_finaldf = shop_scores_finaldf.append(dftmp)

        shop_scores_finaldf.columns = ['score']
        shop_scores_finaldf = pd.merge(shop_scores_finaldf, shop_scores_o, on='store_code')
        shop_scores_finaldf['final_score'] = shop_scores_finaldf['score'] * shop_scores_finaldf['score_overall']

        self.shop_scores_final = shop_scores_finaldf


    def preprocess_final_df_for_modeling(self):

        surr_df_cleaned = self.df_surr.set_index('store_code')

        df = pd.merge(surr_df_cleaned, self.shop_scores_final, on='store_code', how='left')
        df = df.drop(['score_overall', 'score'], axis=1)
        feat_cols = list(df.columns)
        feat_cols.remove('final_score')
        self.feat_cols = feat_cols
        df = df.dropna()
        df.to_csv(proj_path / 'data/final_df_for_modeling.csv')

        self.df_final_modeling = df

    def train_test_split_normalize(self):
        self.traindf, self.testdf = train_test_split(self.df_final_modeling, test_size=0.1, random_state=1234)

        transformer = MaxAbsScaler().fit(self.traindf[self.feat_cols])
        self.train_n = transformer.transform(self.traindf[self.feat_cols])
        self.test_n = transformer.transform(self.testdf[self.feat_cols])

    def model_ridge_regression_for_feature_importance(self):

        selector = SelectFromModel(estimator=Ridge()).fit(self.train_n, self.traindf['final_score'])

        imp = selector.estimator_.coef_
        top10 = np.argsort(-imp)[:10]
        print('top 10 features')
        for ii in top10:
            print('Feature: %0s, Score: %.5f' % (self.feat_cols[ii], imp[ii]))
        bot10 = np.argsort(imp)[:10]
        print('bottom 10 features')
        for ii in bot10:
            print('Feature: %0s, Score: %.5f' % (self.feat_cols[ii], imp[ii]))


    def model_ridge_regression_fit_transform(self):
        self.model_ridge_regression_for_feature_importance()

        regr = RidgeCV(alphas=[0.5, 0.7, 1.0, 5.0, 10.0], cv=3)
        regr.fit(self.train_n, self.traindf['final_score'])
        y_ridgcv = regr.predict(self.test_n)
        self.testdf['pred_ridgcv'] = y_ridgcv
        plt.clf()
        sns.scatterplot(x='final_score', y='pred_ridgcv', data=self.testdf)
        print('RMSE from RidgeCV : ', np.sqrt(mean_squared_error(self.testdf['final_score'], self.testdf['pred_ridgcv'])))

        plt.show()

    def __call__(self, *args, **kwargs):

        print(self.df_surr.head())
        self.score_stores()
        self.preprocess_final_df_for_modeling()
        self.train_test_split_normalize()
        self.model_ridge_regression_fit_transform()





if __name__ == '__main__':

    sales_fpath = proj_path / 'data/sales_granular.csv'
    surr_fpath = proj_path / 'data/Surroundings.json'

    pmju = PMJUseCase(sales_fpath, surr_fpath, sparse_stores_thresh=20)
    pmju()
